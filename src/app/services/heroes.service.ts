import { Injectable } from "@angular/core";

@Injectable()
export class HeroesService{
    
    private heroes:Heroe[] = [
        {
          nombre: "Credimax",
          bio: "Realiza compras, a baja tasas de interes y por ser uno de los 100 primeros en adquirirla en este mes no pagaras cuota de manejo.",
          img: "assets/img/credimax.png",
          aparicion: "2019-05-05",
          casa:"DC",
          tasa: 1
        },
        {
          nombre: "GoCredit",
          bio: "Si tu deseo es viajar por el mundo, esta es tu oportunidad para explorarlo..",
          img: "assets/img/tarjeta2.png",
          aparicion: "2018-01-01",
          casa:"DC",
          tasa: 1.5
        },        
        {
          nombre: "Golden card",
          bio: "Realiza reservas para hoteles, automoviles, restaurantes y disfruta de tus vacaciones soñadas, estas a un click.",
          img: "assets/img/tarjeta3.png",
          aparicion: "2017-06-12",
          casa:"DC",
          tasa: 1.8
        },        
        {
          nombre: "Easy Credit",
          bio: "Cumple tus metas y amplia tu perfil profesional.",
          img: "assets/img/tarjeta4.png",
          aparicion: "2018-04-05",
          casa:"DC",
          tasa: 1.7
        },        
        {
          nombre: "Credit Blue",
          bio: "50% de descuento en tu cuota de manejo.",
          img: "assets/img/tarjeta5.png",
          aparicion: "2019-05-05",
          casa:"DC",
          tasa: 1.8
        },        
        {
          nombre: "Credit Orange",
          bio: "Acceso a salas VIP en más de 600 aeropuertos con Priority Pass, Seguro contra robos en cajeros automáticos, Garantía extendida por 12 meses más en tus compras.",
          img: "assets/img/tarjeta6.png",
          aparicion: "2016-01-01",
          casa:"DC",
          tasa: 1.7
        }        
      ];

      constructor(){
        console.log("Listo para usar!");
    }

    getHeroes():Heroe[]{
        return this.heroes;
    }

    getHeroe(id:number):Heroe{
        return this.heroes[id];
    }

    buscarHeroes( termino:string): Heroe[]{
      let heroeArr:Heroe[] = [];
      termino = termino.toLowerCase();

      //for (let heroe of this.heroes){
      for (let i = 0; i < this.heroes.length; i++){
        let heroe = this.heroes[i];
        let nombre = heroe.nombre.toLowerCase();
        if ( nombre.indexOf( termino ) >= 0){
          heroe.idx=i;
          heroeArr.push (heroe)
        }
      }
      return heroeArr;
    }
}

export interface Heroe{
    nombre: string;
    bio: string;
    img: string;
    aparicion: string;
    casa: string;
    idx?: number;
    tasa: number;
}