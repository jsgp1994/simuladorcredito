import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { HeroesService } from '../../services/heroes.service';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styles: []
})
export class HeroeComponent implements OnInit {

  heroe:any = {};
  show_div: boolean = false;
  error_show:boolean = false;
  iteradorCuotas = [];
  
  constructor( private activatedRoute: ActivatedRoute, private _heroesService: HeroesService) {
    this.activatedRoute.params.subscribe( params => {
      this.heroe = this._heroesService.getHeroe(params['id']);
      //console.log(this.heroe);
    })
   }

  ngOnInit() {
  }

  calcularCuotas() {

    let nCuotas: number = parseFloat((document.getElementById("numCuotas") as HTMLInputElement).value);
    let vMonto: number = parseFloat((document.getElementById("monto") as HTMLInputElement).value);

    if(!Number.isNaN(nCuotas) && !Number.isNaN(vMonto)){
      this.show_div = true;
      this.error_show = false;
      this.iteradorCuotas = [];
  
      let fCuota:number = Math.round(((this.heroe.tasa/100)*vMonto) / (1-(Math.pow(1+(this.heroe.tasa/100),-nCuotas))));      
      let fInteres: number = Math.round(((fCuota * nCuotas) - vMonto)/nCuotas);
  
      for (let index = 0; index < nCuotas; index++) {
        this.iteradorCuotas.push({ id: index+1, cuota: fCuota.toLocaleString(), interes: fInteres.toLocaleString() });     
      }      
    }else{
      this.error_show = true;
      this.show_div = false;
    }         
    
  }  

}
